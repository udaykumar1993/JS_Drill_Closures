const cacheFunction = require('../cache')

function mulArguments(num) {
    return num * num
}

let argumentUsed = cacheFunction(mulArguments);

console.log(argumentUsed(5))
console.log(argumentUsed(6))
console.log(argumentUsed(7))
console.log(argumentUsed(5))
console.log(argumentUsed(5))
console.log(argumentUsed(6))
console.log(argumentUsed(7))

