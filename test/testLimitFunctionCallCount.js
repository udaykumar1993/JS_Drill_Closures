const limitFunctionCallCount = require('../limitFunctionCallCount')

function callback() {
    console.log("callback called")
}

let limit = 4

let nTimes = limitFunctionCallCount(callback, limit);
nTimes.invoke();
nTimes.invoke();
nTimes.invoke();
nTimes.invoke();
nTimes.invoke();
nTimes.invoke();


