const counterFactory = require('../counterFactory.js')

let count = 0

let counter = counterFactory(count)
console.log(counter.value())
counter.increment()
counter.increment()
console.log(counter.value())
counter.decrement()
console.log(counter.value())