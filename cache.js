function cacheFunction(cb) {
    let cache = new Set()
    return function (num) {
        if (cache.has(num)) {
            return cache
        } else {
            cache.add(cb(num))
            return cache;
        }

    };
}
module.exports = cacheFunction;



