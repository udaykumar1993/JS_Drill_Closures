function counterFactory(count) {

    function changeBy(val) {
        count += val;
    }

    return {
        increment: function () {
            changeBy(1);
        },
        decrement: function () {
            changeBy(-1);
        },

        value: function () {
            return count;
        }
    }
};



module.exports = counterFactory;




