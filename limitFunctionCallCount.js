function limitFunctionCallCount(callback, n) {

    let counter = n;
    function invoke() {
        if (counter-- > 0) {
            callback()
        } else {
            console.log(`Callback already called ${n}-times`);
        }

    }
    return { invoke }

}

module.exports = limitFunctionCallCount






